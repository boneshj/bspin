module BspinHelper

  def styling(options = {})

    options[:colour] = options[:color] if options.has_key?(:color)
    options[:colour] ||= 'black'
    options[:type] ||= 'ball'
    options[:type] = "ball" if !["ball", "circle1", "circle1-fade", "circle2",
                                 "bubble1", "bubble2", "bar1", "bar2"].include? options[:type]

    colour = Color::CSS[options[:colour]]
    colour = Color::RGB.from_html(options[:colour]) if colour.blank?
    red = colour.red.to_i
    green = colour.green.to_i
    blue = colour.blue.to_i

    sizing = ( options[:type] == "ball" ? "1" : "2" )

    case options[:size]
      when "small"
        options[:size] = ( sizing == "1" ? 30 : 5 )
      when "medium"
        options[:size] = ( sizing == "1" ? 50 : 11 )
      when "large"
        options[:size] = ( sizing == "1" ? 90 : 18 )
      when "x-large"
        options[:size] = ( sizing == "1" ? 120 : 25 )
      else
        options[:size] = ( sizing == "1" ? 90 : 18 )
    end

    case options[:speed]
      when "slow"
        options[:speed] = 3.0
      when "regular"
        options[:speed] = 1.7
      when "fast"
        options[:speed] = 0.5
      else
        options[:speed] = 1.7
    end

    render partial: 'bspin/bspin', locals: { colour: options[:colour],
                                             size: options[:size],
                                             speed: options[:speed],
                                             spin_type: options[:type],
                                             red: red,
                                             green: green,
                                             blue: blue }
  end
end