# Bspin
[![Gem Version](https://badge.fury.io/rb/bspin.svg)](http://badge.fury.io/rb/bspin)

This will allow a call to a bspin - a css spinner styled to sit in the middle of the page over text!

## Installation

Add these lines to your application's Gemfile:

```ruby
gem 'bspin'
gem 'color'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install bspin

## Usage

in your header have:
  <style><%= styling -%></style>

 An example call is <div class='bspin'></div>
 
 Styling has several options:

 type:  "ball" | "circle1" | "circle1-fade" | "circle2" | "bubble1" | "bubble2" | "bar1" | "bar2" - default is "ball"

 colour: any hex value or defined CSS colour value - default is black.

 size: "small" | "medium" | "large" | "x-large" - default is "medium".

 speed: "slow" | "regular" | "fast" - default is "regular".
 
 Example: <%= styling(type: "circle1", colour: "#000000", size: "large", speed: "regular") -%>


## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on BitBucket at https://bitbucket.org/boneshj/bspin. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](contributor-covenant.org) code of conduct.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).